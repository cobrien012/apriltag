#include "apriltag.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <iomanip>
#include <array>

using namespace std;

#define PI 3.14159265358979323846

void print_mat(cv::Mat_<double> print) {
  std::cout << std::fixed << std::setprecision(5);
  for (int n = 0; n < print.rows; n++) {
    for (int m = 0; m < print.cols; m++) {
      std::cout << (print(n,m) >= 0 ? std::setprecision(5) : std::setprecision(4));
      std::cout << print(n,m) << ", ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

cv::VideoCapture open_firefly(void) {
  cv::VideoCapture cap(0);
  // cap.set(CV_CAP_PROP_FRAME_WIDTH, 752);
  // cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
  cap.set(CV_CAP_PROP_FPS, 60);
  // cap.set(CV_CAP_PROP_EXPOSURE, 255);
  return cap;
}

void pose_to_RPY(const cv::Mat_<double> R, std::array<double, 3>& euler) {
  euler[2] = atan2(R(1,0), R(0,0));
  euler[1] = atan2(-1*R(2,0), sqrt(R(2,1)*R(2,1) + R(2,2)*R(2,2)));
  euler[0]  = atan2(R(2,1), R(2,2));
}

void pose_to_RPYd(const cv::Mat_<double> R, std::array<double, 3>& euler) {
  pose_to_RPY(R, euler);
  euler[0] = euler[0]*180.0/PI;
  euler[1] = euler[1]*180.0/PI;
  euler[2] = euler[2]*180.0/PI;
}

int main(int argc, char** argv) {

  double yaw, pitch, roll;

  cv::namedWindow("TEST");
  cv::Mat_<double> pose(4,4);
  cv::Mat img;
  cv::Mat gray;

  cv::VideoCapture cap = open_firefly();

  if(!cap.isOpened()) {
    cout << "Camera Not Found!" << endl;
    return 0;
  } else {
    cout << "Camera Opened ..." << endl;
  }

  int w = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  int h = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

  apriltag::detector at(w, h, 2.4);
  at.set_quad_decimate(4);
  at.set_nthreads(10);

  array<double, 3> euler;
  int i;
  int key;
  double last_frame = apriltag::cv_tic();
  while (true) {
    i++;
    cap >> img;
    if (img.channels() != 1) {
      cv::cvtColor(img, gray, CV_BGR2GRAY);
      pose = at.compute_position(gray);
    } else {
      pose = at.compute_position(img);
    }

    pose_to_RPYd(pose, euler);
    cout << std::fixed << std::setprecision(5) << euler[0] << " , " << euler[1] << " , " << euler[2] << endl;
    // print_mat(pose);

    if (apriltag::cv_tic() - last_frame > 0.1) {
      last_frame = apriltag::cv_tic();
      // cout << "X: " << pose(0,3) << " , Y: " << pose(1,3) << " , Z: " << pose(2,3) << endl;
      if (img.channels() == 1)
        cv::cvtColor(gray, img, CV_GRAY2BGR);
      at.draw(img);
      cv::imshow("TEST", img);
      key = cv::waitKey(1);
    if (key == 27)
      break;
    if (key == 114) {
      cout << "Restarting Camera ..." << endl;
      cap.release();
      cv::waitKey(100);
      cap = open_firefly();
    }    
  }
  }
  return 0;
}
