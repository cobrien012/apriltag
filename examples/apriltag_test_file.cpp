#include "apriltag.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <iomanip>
#include <array>

using namespace std;
using namespace cv;

int main(int argc, char** argv) {
  if( argc != 2) {
   cout <<" Usage: display_image ImageToLoadAndDisplay" << endl;
   return -1;
  }

  Mat img = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
  if(!img.data) {
    cout <<  "Could not open or find the image" << endl ;
    return -1;
  }

  apriltag::detector at;
  at.tag_size = 2.4;
  at.set_quad_decimate(4);
  at.set_nthreads(10);

  Mat_<double> pose(4,4);

  if (img.channels() != 1) {
    Mat gray;
    cvtColor(img, gray, CV_BGR2GRAY);
    pose = at.compute_position(gray);
  } else {
    pose = at.compute_position(img);
  }

  at.draw(img);

  namedWindow("TEST");
  imshow("TEST", img);
  waitKey(-1);

  return 0;
}
