#include "apriltag.hpp"

#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <cmath>
#include <iostream>
#include <sstream>

#include "apriltag_c/apriltag.h"
#include "apriltag_c/tag36h11.h"

using namespace apriltag;
using namespace std;
using namespace cv;

// Conversion functions
void cvimage_to_imageu8(const cv::Mat &img_in, image_u8_t *img_out) {
  // Copy by rows, image_u8_t is rarely contiguous, so use array stride for row pointer
  const uchar* p;
  for (int row = 0; row < img_in.rows; ++row) {
    p = img_in.ptr<uchar>(row);
    memcpy(&img_out->buf[row*img_out->stride], p, img_in.cols*sizeof(uint8_t));
  }
}

cv::Mat matd_to_opencv(const matd_t *mat) {
  cv::Mat_<double> cv_mat(mat->nrows, mat->ncols);
  for (int n = 0; n < cv_mat.rows; n++) {
    for (int m = 0; m < cv_mat.cols; m++) { 
      cv_mat(n,m) = MATD_EL(mat,n,m);
    }
  }
  return cv_mat;
}

// Apriltag detector
namespace {
  apriltag_detector_t *tag_det;
  apriltag_family_t *tag_fam;
  zarray_t *detections;

  image_u8_t *img_det;

  char fps_text[10];
  double last_t;
  int font;
  double font_scale;
  int thickness;
  cv::Point fps_origin;
}

detector::detector(int new_width, int new_height, double new_tag_size) :
frame(0),
fps(0),
img_width(0),
img_height(0)
{
  // Initialize Apriltag objects
  detections = zarray_create(sizeof(apriltag_detection_t));
  tag_det = apriltag_detector_create();
  tag_fam = tag36h11_create();
  apriltag_detector_add_family(tag_det, tag_fam);

  // Preallocate image matrices
  img = cv::Mat(img_height, img_width, CV_8UC3, cv::Scalar(0,0,0));
  img_gray = cv::Mat(img_height, img_width, CV_8U, 0);
  img_det = image_u8_create(img_width, img_height);

  font = cv::FONT_HERSHEY_SIMPLEX;
  font_scale = 1;
  thickness = 2;
  last_t = cv_tic();
}

detector::~detector(void) {
  apriltag_detector_destroy(tag_det);
  tag36h11_destroy(tag_fam);
  zarray_destroy(detections);
  image_u8_destroy(img_det);
}

void detector::detect_tags(const cv::Mat& img_in) {

  if (img_in.rows != img_height || img_in.cols != img_width) {
    img_height = img_in.rows;
    img_width = img_in.cols;
    image_u8_destroy(img_det);
    img_det = image_u8_create(img_width, img_height);
  }

  if (img_in.channels() == 3) {
    cv::cvtColor(img_in, img_gray, CV_BGR2GRAY);
    cvimage_to_imageu8(img_gray, img_det);
  }
  else
    cvimage_to_imageu8(img_in, img_det);

  zarray_destroy(detections);
  detections = apriltag_detector_detect(tag_det, img_det);

  tag_centers.clear();
  apriltag_detection_t *det;

  for (int i = 0; i < zarray_size(detections); i++) {
    zarray_get(detections, i, &det);
    tag_centers.push_back(cv::Point_<double>(det->c[0], det->c[1]));
  }

  if (zarray_size(detections) > 0)
    detected = true;
  else
    detected = false;

  calc_fps();
}

void detector::calc_fps() {
  ++frame;
  if (cv_tic() - last_t > 0.25) {
    fps = frame/(cv_tic() - last_t);
    sprintf(fps_text, "%04.2f FPS", fps);
    last_t = cv_tic();
    frame = 0;
  }
}

cv::Mat_<double> detector::compute_position(const cv::Mat& img_in) {
  apriltag::detector::detect_tags(img_in);
  return detector::compute_position();
}

cv::Mat_<double> detector::compute_position(void) {
  if (zarray_size(detections) == 0)
    return cv::Mat_<double>::zeros(4,4);

  apriltag::tag det_tag;

  // correspondences is a zarray list of float[4], where each element is {a,b,c,d},
  // such that y = Hx, x = [a,b], y = [c d]
  zarray_t *correspondences = zarray_create(sizeof(float[4]));
  apriltag_detection_t *det;

  for (int tag = 0; tag < detections->size; tag++) {
    zarray_get(detections, tag, &det);

    // If no tags have been specified, use ideal and skip rest
    if (tag_info.empty()) {
      det_tag = apriltag::tag({0,0},tag_size);
      tag = detections->size;
    }
    // If tags are specified, but this one isnt in map, skip and notify
    else if (!tag_info.count(det->id)) { 
      std::cout << "Unknown Tag! Skipping it!" << std::endl;
      continue;
    }
    // else load appropriate tag from map
    else
      det_tag = tag_info[det->id];

    // add center point
    // corr_point[0] = det_tag.center[0];
    // corr_point[1] = det_tag.center[1];
    // corr_point[2] = det->c[0];
    // corr_point[3] = det->c[1];
    // zarray_add(correspondences, &corr_point);

    // add corner points
    for (int i = 0; i < 4; i++) {
      float corr_point[4];

      corr_point[0] = det_tag.corners[i][0];
      corr_point[1] = det_tag.corners[i][1];
      corr_point[2] = det->p[i][0];
      corr_point[3] = det->p[i][1];
      zarray_add(correspondences, &corr_point);
      // cout << "( " << det_tag.corners[i][0] << " , " << det_tag.corners[i][1] << " )";
      // cout << "( " << det->p[i][0] << " , " << det->p[i][1] << " )";
    }
  }
  // cout << correspondences->size << endl;

  matd_t *H = homography_compute(correspondences, HOMOGRAPHY_COMPUTE_FLAG_SVD);
  matd_t *Hinv = matd_inverse(H);
  matd_t *pose = homography_to_pose(det->H, 1100, 1100, 276, 248);
  cv::Mat_<double> pose_cv = matd_to_opencv(pose);

  zarray_destroy(correspondences);
  matd_destroy(H);
  matd_destroy(pose);

  return pose_cv;
}

void detector::draw(cv::Mat img_in, bool draw_fps) {
  int color;
  if (img_in.type() == CV_8UC3)
    color = 255;
  else if (img_in.type() == CV_16UC3)
    color = 65536;
  else
    color = 255;

  apriltag_detection_t *det;

  for (int i = 0; i < zarray_size(detections); i++) {
    zarray_get(detections, i, &det);
    cv::putText(img_in, std::to_string(det->id),
      cv::Point(det->c[0]+5, det->c[1]+5),
      cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 0, 255), 2, 8, false);

    cv::Point center(det->c[0], det->c[1]);
    cv::Point p0(det->p[0][0], det->p[0][1]);
    cv::Point p1(det->p[1][0], det->p[1][1]);
    cv::Point p2(det->p[2][0], det->p[2][1]);
    cv::Point p3(det->p[3][0], det->p[3][1]);


    cv::line(img_in, p0, p1, cv::Scalar(color, 0, color), 2);
    cv::line(img_in, p1, p2, cv::Scalar(color, 0, color), 2);
    cv::line(img_in, p2, p3, cv::Scalar(color, 0, color), 2);
    cv::line(img_in, p3, p0, cv::Scalar(color, 0, color), 2);

    int radius = 3;
    int thickness = -1;

    cv::circle(img_in, center, radius, cv::Scalar(0, 0, color), thickness);
    cv::circle(img_in, p0, radius, cv::Scalar(color,   0,   0), thickness);
    cv::circle(img_in, p1, radius, cv::Scalar(  0, color,   0), thickness);
    cv::circle(img_in, p2, radius, cv::Scalar(color, color,   0), thickness);
    cv::circle(img_in, p3, radius, cv::Scalar(  0, color, color), thickness);
  }
  if (draw_fps)
  cv::putText(img_in, fps_text, cv::Point(10, img_in.rows-10),
    font, font_scale, cv::Scalar(0, color, 0),
    thickness, 8, false);
}

void detector::set_quad_decimate(int scale) {
  tag_det->quad_decimate = scale;
}

void detector::set_nthreads(int nthreads) {
  tag_det->nthreads = nthreads;
}
void detector::load_tags(std::string f_id) {
  std::ifstream infile(f_id);

  int tag_id;
  float x, y, size;

  std::string line;
  std::stringstream ss;
  while (std::getline(infile, line)) {
    if (line[0] != '#') {
      ss.str(line);
      ss.clear();
      ss >> tag_id
        >> x >> y
        >> size;
      std::cout << "Tag ID:" << tag_id
      << ", X: " << x
      << ", Y: " << y
      << ", Size: " << size << std::endl;

      if (!tag_info.count(tag_id))
        tag_info.insert({tag_id, tag({x,y},size)});
      else
        std::cout << "Tag already specified! Check config file" << std::endl;
    }
  }
}

