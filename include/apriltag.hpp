#ifndef _APRILTAG_
#define _APRILTAG_

#include <opencv2/core/core.hpp>

#include <array>
#include <string>
#include <vector>
#include <unordered_map>

namespace apriltag {

inline double cv_tic(void) {
  return cv::getTickCount()/cv::getTickFrequency();
}

class tag {
public:
  std::array< std::array<double, 2>, 4> corners; 
  std::array<double, 2> center; 
  double size;
  double angle;
  tag(std::array<double, 2> center_in = {0,0}, double size_in = 1) {
    center = center_in;
    size = size_in;
    for (int i = 0; i < 4; i++) {
      corners[i][0] = center[0] + ((i == 0 || i == 3) ? -size/2 : size/2);
      corners[i][1] = center[1] + ((i == 0 || i == 1) ? -size/2 : size/2);
    }
  }
};



class detector {
 public:

  // image variables
  int img_width;
  int img_height;
  cv::Mat img;
  cv::Mat img_gray;

  // Variables for locationfinding
  std::unordered_map<int, tag> tag_info;
  double tag_size;

  // frame rate record and overlay
  int frame;
  float fps;

  // Control logic
  bool detected;
  
  std::vector<cv::Point_<double>> tag_centers;

  // Default Constructor
  detector(int new_width = 1280, int new_height = 720, double new_tag_size = 2.4);

  ~detector(void);

  // Sets resolution scale to look for apriltags
  void set_quad_decimate(int scale);

  // Sets number of threads used for processing
  void set_nthreads(int n_threads);

  // Load tag location and sizes from text file
  void load_tags(std::string f_id);

  // Detect apriltags in color or gray opencv image
  void detect_tags(const cv::Mat& img_in);
  
  // Overlay detections onto image
  void draw(cv::Mat img_in, bool draw_fps = true);

  // Compute relative position using homography
  cv::Mat_<double> compute_position(void); // use last calculated detections
  cv::Mat_<double> compute_position(const cv::Mat& img_in); // do new detection from image
  void calc_fps();
};

}

#endif 
/* _APRILTAG */
