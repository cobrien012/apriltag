from cv2 import *
import glob
import os
import sys



cap = VideoCapture(300)

if cap.isOpened():
    old_calib_files = glob.glob("CALIB_IMG_[0-9][0-9].png")
    for old_file in old_calib_files:
        os.remove(old_file)
else:
    print "Camera Not Opened!"
    cap.release()
    sys.exit()

board_size = (10,8)

count = 0;
n_imgs = 20
namedWindow("test", cv.CV_WINDOW_NORMAL)
while True:
    ret, frame = cap.read()
    disp_frame = resize(frame, None, 0, 2, 2)
    imshow('test', disp_frame)
    key = waitKey(1)
    if key == 27:
        break
    if key & 0xFF == ord(' '):
        chk_present, corners = findChessboardCorners(frame, board_size, None, CALIB_CB_FAST_CHECK)

        if not chk_present:
            print "No checkerboard detected"
            continue

        print "good?"
        if waitKey() & 0xFF == ord(' '):
            print "accepted #%s" % str(count).zfill(2)
            imwrite("CALIB_IMG_%s.png" % str(count).zfill(2), cvtColor(frame, COLOR_GRAY2BGR))
            count = count + 1
            if count >= n_imgs:
                break
        else:
            print "rejected"

cap.release()
destroyAllWindows()