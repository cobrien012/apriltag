from cv2 import *
import numpy as np
import glob
import pdb
import os
import sys

cap = VideoCapture(300)
cap.set(cv.CV_CAP_PROP_FPS, 1)

if cap.isOpened():
    old_calib_files = glob.glob("CALIB_IMG_[0-9][0-9].png")
    for old_file in old_calib_files:
        os.remove(old_file)
else:
    print "Camera Not Opened!"
    cap.release()
    sys.exit()

criteria = (TERM_CRITERIA_EPS + TERM_CRITERIA_MAX_ITER, 30, 0.001)

objp = np.zeros((10*8,3), np.float32)
objp[:,:2] = 2*np.mgrid[0:10,0:8].T.reshape(-1,2)

objpoints = []
imgpoints = []

board_size = (10,8)
calib_images = []

count = 0
while True:
    ret, gray = cap.read()

    if waitKey(1) == 27:
        cap.release()
        sys.exit()

    ret_corn, corners = findChessboardCorners(gray, board_size, None, CALIB_CB_FAST_CHECK)

    if ret_corn == True:
        imwrite("CALIB_IMG_%s.png" % str(count).zfill(2), gray)
        calib_images.append(gray)

        objpoints.append(objp)
        ret_corn, corners = findChessboardCorners(gray, board_size)

        cornerSubPix(gray,corners,(5,5),(-1,-1),criteria)
        imgpoints.append(corners)

        drawChessboardCorners(gray, board_size, corners, ret_corn)

        # Draw and display the corners
        count = count + 1
        if count > 25:
            break
    
    imshow('img', gray)

    for i in range(0, 2):
        ret, gray = cap.read()


print "Finished Acquiring, Calibrating..."
ret, camera_matrix, dist_coeffs, rvecs, tvecs = calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None,None,None,None)

if ret:
    print camera_matrix
    print dist_coeffs

saved_mat = np.concatenate((camera_matrix.reshape((-1,1)),dist_coeffs.reshape((-1,1))))
np.savetxt('camera_calibration.txt', saved_mat, fmt='%15.10f')

for gray in calib_images:
    corr_img = undistort(gray, camera_matrix, dist_coeffs);
    imshow('img', np.hstack((gray, corr_img)))
    waitKey(100)