from cv2 import *
import numpy as np
import glob
import pdb
calib_files = glob.glob("CALIB_IMG_[0-9][0-9].png")

criteria = (TERM_CRITERIA_EPS + TERM_CRITERIA_MAX_ITER, 100, 0.001)

objp = np.zeros((10*8,3), np.float32)
objp[:,:2] = 2*np.mgrid[0:10,0:8].T.reshape(-1,2)

objpoints = []
imgpoints = []

board_size = (10,8)
for calib_img in calib_files:
    print calib_img
    img = imread(calib_img)
    gray = cvtColor(img,COLOR_BGR2GRAY)

    ret, corners = findChessboardCorners(gray, board_size)

    if ret == True:
        objpoints.append(objp)

        cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners)

        # Draw and display the corners
        drawChessboardCorners(img, board_size, corners, ret)
        imshow('img', img)
        waitKey(50)

ret, camera_matrix, dist_coeffs, rvecs, tvecs = calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

if ret:
    print camera_matrix
    print dist_coeffs

saved_mat = np.concatenate((camera_matrix.reshape((-1,1)),dist_coeffs.reshape((-1,1))))
np.savetxt('camera_calibration.txt', saved_mat, fmt='%15.10f')

for calib_img in calib_files:
    img = imread(calib_img)
    corr_img = undistort(img, camera_matrix, dist_coeffs)
    imshow('img', np.hstack((corr_img, img)))
    waitKey(50)

cap = VideoCapture(300)

while True:
    ret, frame = cap.read()
    corr_frame = undistort(frame, camera_matrix, dist_coeffs)
    imshow('img', np.hstack((corr_frame, frame)))
    waitKey(1)

cap.release()

